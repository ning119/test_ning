import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-test-three',
  templateUrl: './test-three.component.html',
  styleUrls: ['./test-three.component.css']
})
export class TestThreeComponent {

  area1:any;


  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    let base = form.value.base
    let height = form.value.height
    let area = 0.5 * base * height

    this.area1 = area
    console.log(area);
}





}
