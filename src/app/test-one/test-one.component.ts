import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-test-one',
  templateUrl: './test-one.component.html',
  styleUrls: ['./test-one.component.css']
})
export class TestOneComponent {
  value2: any;
  constructor(private titleService:Title) {
  }

  ngOnInit() {

  }



  onItemSelector(value :string) {
    this.value2 = value;
    console.log(value);
    }

}
