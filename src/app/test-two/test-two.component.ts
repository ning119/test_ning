import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-test-two',
  templateUrl: './test-two.component.html',
  styleUrls: ['./test-two.component.css']
})
export class TestTwoComponent {
  posts:any;

  constructor(private titleService:Title,
    private service:PostService,
    ) {
  }

  ngOnInit() {

    this.service.getPosts()
    .subscribe(response => {
      this.posts = response;
    });
  }

}
