import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestOneComponent } from './test-one/test-one.component';
import { TestTwoComponent } from './test-two/test-two.component';
import { TestThreeComponent } from './test-three/test-three.component';
import { TestFourComponent } from './test-four/test-four.component';
import { TestFiveComponent } from './test-five/test-five.component';

const routes: Routes = [
  {
    path: 'hero',
    component: TestOneComponent,
    title: "hero"
  },
  {
    path: 'posts',
    component: TestTwoComponent,
    title: "posts"
  },
  {
    path: 'test-three',
    component: TestThreeComponent,
    title: "test-three"
  },
  {
    path: 'test-four',
    component: TestFourComponent,
    title: "test-four"
  },
  {
    path: 'test-five',
    component: TestFiveComponent,
    title: "test-five"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
