import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PostService } from './services/post.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'title Service Example';
  posts: any;

  constructor(private titleService:Title,
    private service:PostService) {
  }

  ngOnInit() {
    this.titleService.setTitle("hero");


  }
}
